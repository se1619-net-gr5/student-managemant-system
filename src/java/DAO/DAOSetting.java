///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package dao;
//
//import entity.Setting;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.ArrayList;
//
///**
// *
// * @author FPT SHOP
// */
//public class DAOSetting {
//
//    Connection connection = DBConnection.open();
//
//    public ArrayList<Setting> listSetting() {
//        ArrayList<Setting> list = new ArrayList<>();
//        String sql = "select * from setting";
////        ResultSet rs = getData(sql);
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                int setting_id = rs.getInt(1);
//                String setting_name = rs.getString(2);
//                String setting_value = rs.getString(3);
//                int type_id = rs.getInt(4);
//                int order = rs.getInt(5);
//                String note = rs.getString(6);
//                String status = rs.getString(7);
//                
//                Setting st = new Setting(setting_id, setting_name, setting_value, type_id, order, note, status);
//                list.add(st);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
//
//    public void addSetting(String name, String sValue, int tId, int order, String note, String status) {
//        String sql = "INSERT INTO `manager_project`.`setting`\n"
//                + "(`setting_name`,\n"
//                + "`setting_value`,\n"
//                + "`type_id`,\n"
//                + "`orders`,\n"
//                + "`note`,\n"
//                + "`status`)\n"
//                + "VALUES\n"
//                + "(?,\n"
//                + "?,\n"
//                + "?,\n"
//                + "?,\n"
//                + "?,\n"
//                + "?);";
//
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setString(1, name);
//            ps.setString(2, sValue);
//            //xem lai type id
//            ps.setInt(3, tId);
//            ps.setInt(4, order);
//            ps.setString(5, note);
//            ps.setString(6, status);
//            int n = ps.executeUpdate();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void updateSetting(int id, String name, String sValue, int tId, int order, String note, String status) {
//        String sql = "UPDATE `manager_project`.`setting`\n"
//                + "SET\n"
//                + "`setting_name` = ?,\n"
//                + "`setting_value` = ?,\n"
//                + "`type_id` = ?,\n"
//                + "`orders` = ?,\n"
//                + "`note` = ?,\n"
//                + "`status` = ?\n"
//                + "WHERE `setting_id` = ?;";
//        try {
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setString(1, name);
//            ps.setString(2, sValue);
//            ps.setInt(3, tId);
//            ps.setInt(4, order);
//            ps.setString(5, note);
//            ps.setString(6, status);
//            ps.setInt(7, id);
//            int n = ps.executeUpdate();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    public Setting getSetting(int id) {
//        String sql = "select * from setting where setting_id = ?";
//        Setting st = null;
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, id);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                int setting_id = rs.getInt(1);
//                String setting_name = rs.getString(2);
//                String setting_value = rs.getString(3);
//                int type_id = rs.getInt(4);
//                int order = rs.getInt(5);
//                String note = rs.getString(6);
//                String status = rs.getString(7);
//                st = new Setting(setting_id, setting_name, setting_value, type_id, order, note, status);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return st;
//    }
//
////    public ArrayList<setting> SearchByName(String name) {
////        ArrayList<setting> list = new ArrayList<>();
////        String sql = "select * from setting where setting_name like '%" + name + "%'";
////
////        try {
////            PreparedStatement ps = conn.prepareStatement(sql);
////            ResultSet rs = ps.executeQuery();
////            while (rs.next()) {
////                int setting_id = rs.getInt(1);
////                String setting_name = rs.getString(2);
////                String setting_value = rs.getString(3);
////                int type_id = rs.getInt(4);
////                int order = rs.getInt(5);
////                String note = rs.getString(6);
////                setting st = new setting(setting_id, setting_name, setting_value, type_id, order, note);
////                list.add(st);
////            }
////
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        return list;
////    }
//    public static void main(String[] args) {
//        DAOSetting dao = new DAOSetting();
//        ArrayList<Setting> list = dao.listSetting();
//        //System.out.println(list.size());
////        for (setting obj : list) {
////            System.out.println(obj.toString());
////        }
//        //dao.addSetting("Video Lesson", "Ntg", 2, 1, "no");
//
//        //dao.updateSetting(st);
//        dao.updateSetting(2, "Quan ly du an", "15", 2, 1, "no", "Inactive");
//    }
//}
