/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author DoXuanHieu
 */
public class SendEmail {

    private String userEmail;
    private String hash;

    public SendEmail(String userEmail, String hash) {
        super();
        this.userEmail = userEmail;
        this.hash = hash;
    }

    public void sendMail() {
        String email = "doxuanhieufptu@gmail.com"; // sender email
        String password = "<!x=s\")KTDx'&P6s"; // sender password

        Properties properties = new Properties();

        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(email, password);
            }
        });

        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));
            message.setSubject("SMS – Verify your account");
            message.setText("We’re excited you’ve joined Student Management System.\n"
                    + "As soon as you verify your email to confirm this is you, we can get started.\n"
                    + "\n"
                    + "Just click the link below:\n"
                    + "http://localhost:8084/MangerProject1/AccountActiveController?email=" + userEmail + "&hash=" + hash
                    + "\nLink will expire in 24 hours.\n"
                    + "The Student Management System team.");
            Transport.send(message);

        } catch (Exception e) {

            System.out.println("SendEmail File Error" + e);
        }
    }
}
