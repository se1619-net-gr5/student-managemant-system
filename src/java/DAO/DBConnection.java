/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KienLT
 */
public class DBConnection {
    
    private static final String SERVER_NAME = "localhost";
    private static final String DB_NAME = "manager_project";
    private static final String PORT_NUMBER = "3306";
    private static final String INSTANCE ="";//LEAVE THIS ONE EMPTY IF YOUR SQL IS A SINGLE INSTANCE
    private static final String USER_ID = "root";
    private static final String PASSWORD = "admin";
    //jdbc:mysql://localhost:3306/manager_project?zeroDateTimeBehavior=convertToNull
    
    private static String URL = "jdbc:mysql://"+SERVER_NAME+":"+PORT_NUMBER +"/"+DB_NAME+"?zeroDateTimeBehavior=convertToNull";
    
    public static Connection open() {
        try {
            //1. Đăng ký driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2. Tạo và mở kết nối
            return DriverManager.getConnection(URL,USER_ID,PASSWORD);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void close(Connection conn, Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
