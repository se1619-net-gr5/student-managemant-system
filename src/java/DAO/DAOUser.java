/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import entity.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FPT SHOP
 */
public class DAOUser {

    Connection connection = DBConnection.open();

    public String addUser(User user) {

        String fullname = user.getFullName();
        String email = user.getEmail();
        String password = user.getPassword();
        String hash = user.getHash();

        PreparedStatement pstmt;

        try {
            pstmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
            pstmt.setString(1, email);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                String checkEmail = rs.getString("email");

                if (email.equals(checkEmail)) {
                    return "sorry email already exist";
                }
            } else {
                pstmt = connection.prepareStatement("INSERT INTO `manager_project`.`user` (`full_name`,"
                        + " `email`, `password`, `hash`) VALUES (?, ?, ?, ?);");

                pstmt.setString(1, fullname);
                pstmt.setString(2, email);
                pstmt.setString(3, password);
                pstmt.setString(4, hash);

                int i = pstmt.executeUpdate();

                if (i != 0) {
                    SendEmail se = new SendEmail(email, hash);
                    se.sendMail();
                    return "SUCCESS";
                }
            }
        } catch (Exception e) {
            System.out.println("RegisterDao File Error" + e);
        }

        return "error";
    }

    public User getUserbyEmailPassWord(String email, String password) {
        String sql = "SELECT * FROM manager_project.user\n"
                + " where email=? and password=?;";
        Connection conn = DBConnection.open();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String fname = rs.getString(2);
                String hash = rs.getString(5);
                int roleid = rs.getInt(6);
                String link = rs.getString(7);
                String status = rs.getString(8);
                User u = new User(id, fname, email, password, hash, roleid, link, status);
                return u;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public User getUserById(int id) {
        String sql = "select * from manager_project.user where user_id =?;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
//                int user_id = rs.getInt(1);
//                String full_name = rs.getString(2);
//                email = rs.getString(3);
//                password = rs.getString(4);
//                String role_id = rs.getString(5);
//                String avarta_link =rs.getString(6);
//                String status = rs.getString(7);
//                User User = new User(user_id, full_name, email, password, role_id, avarta_link, status);
//                return User;

                User u = new User();
                u.setUserId(rs.getInt("user_id"));
                u.setFullName(rs.getString("full_name"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setRoleId(rs.getInt("role_id"));
                u.setAvatarLink(rs.getString("avatar_link"));
                u.setStatus(rs.getString("status"));
                return u;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateUser(User u) {
        String sql = "update manager_project.user set\n"
                + "full_name=?,\n"
                + "email=?,\n"
                + "password=?,\n"
                + "role_id=?,\n"
                + "avatar_link=?,\n"
                + "status=?\n"
                + "where user_id =?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, u.getFullName());
            ps.setString(2, u.getEmail());
            ps.setString(3, u.getPassword());
            ps.setInt(4, u.getRoleId());
            ps.setString(5, u.getAvatarLink());
            ps.setString(6, u.getStatus());
            ps.setInt(7, u.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DAOUser dao = new DAOUser();
//        User user = new User();
//        user.setEmail("xuanhieu901@gmail.com");
//        user.setFullName("Xuan Hieu Do");
//        user.setPassword("@Lanh1109");
//        user.setHash("1234567890");
//        dao.addUser(user);

        System.out.println(dao.getUserbyEmailPassWord("xuanhieu901@gmail.com", "12345678"));
    }
}
