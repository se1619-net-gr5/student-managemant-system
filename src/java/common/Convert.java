/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 *
 * @author Admin
 */
public class Convert {
    public static int toInt(String text)
    {
        int result = 0;
            try
            {
                 result = Integer.parseInt(text);
            }
            catch(NumberFormatException e)
            {
                return -1;
            }
        return result;
    }
}
