/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import common.jbcrypt.BCrypt;

/**
 *
 * @author Admin
 */
public class PasswordEncryption {

    public static String hashPassword(String password) {
        String hashPass = BCrypt.hashpw(password, BCrypt.gensalt(13));
        return hashPass;
    }

//    public static void main(String[] args) {
//        String password = "12345678";
//        System.out.println(hashPassword(password));
//        System.out.println(hashPassword(password));
//        System.out.println(hashPassword(password));
//    }

}
