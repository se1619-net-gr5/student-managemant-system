/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import dao.DAOUser;
import entity.User;
import common.*;
import common.jbcrypt.BCrypt;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class EditProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            DAOUser dao = new DAOUser();
            String text = request.getParameter("id");
            int id = Convert.toInt(text);
            User u = dao.getUserById(id);
            request.setAttribute("id", u.getUserId());
            request.setAttribute("email", u.getEmail());
            request.setAttribute("fullname", u.getFullName());
            request.setAttribute("password", u.getPassword());
            request.setAttribute("status", u.getStatus());
            request.getRequestDispatcher("/editProfile.jsp").forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            DAOUser dao = new DAOUser();
            String salt = BCrypt.gensalt(10);
            String text = request.getParameter("Id");
            int id = Convert.toInt(text);
            User u = dao.getUserById(id);
            String email = request.getParameter("Email");
            String fullName = request.getParameter("Fullname");
            String password = request.getParameter("Password");
            password = BCrypt.hashpw(password, salt);
            String status = request.getParameter("Status"); 
            u.setEmail(email);
            u.setFullName(fullName);
            //u.setPassword(password);
            u.setStatus(status);
            dao.updateUser(u);
            response.sendRedirect("./Profile?id=" + id);
        } catch (Exception ex) {
            request.getRequestDispatcher("/editProfile.jsp").forward(request, response);
//            ex.printStackTrace();
//            throw ex;
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
