/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author FPT SHOP
 */
public class WebContact {

    private int contactId;
    private String fullName;
    private String email;
    private String mobile;
    private int categoryId;
    private String message;
    private String response;
    private String status;

    public WebContact() {
    }

    public WebContact(int contactId, String fullName, String email, String mobile, int categoryId, String message, String response, String status) {
        this.contactId = contactId;
        this.fullName = fullName;
        this.email = email;
        this.mobile = mobile;
        this.categoryId = categoryId;
        this.message = message;
        this.response = response;
        this.status = status;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WebContact{" + "contactId=" + contactId + ", fullName=" + fullName + ", email=" + email + ", mobile=" + mobile + ", categoryId=" + categoryId + ", message=" + message + ", response=" + response + ", status=" + status + '}';
    }

}
