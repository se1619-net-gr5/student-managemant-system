/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author FPT SHOP
 */
public class Setting {

    private int settingId;
    private String settingName;
    private String settingValue;
    private int typeId;
    private int order;
    private String note;

    public Setting() {
    }

    public Setting(int settingId, String settingName, String settingValue, int typeId, int order, String note) {
        this.settingId = settingId;
        this.settingName = settingName;
        this.settingValue = settingValue;
        this.typeId = typeId;
        this.order = order;
        this.note = note;
    }

    public int getSettingId() {
        return settingId;
    }

    public void setSettingId(int settingId) {
        this.settingId = settingId;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "setting{" + "settingId=" + settingId + ", settingName=" + settingName + ", settingValue=" + settingValue + ", typeId=" + typeId + ", order=" + order + ", note=" + note + '}';
    }

}
