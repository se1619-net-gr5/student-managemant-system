<%-- 
    Document   : index
    Created on : May 25, 2022, 8:29:25 PM
    Author     : DoXuanHieu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Management System</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets2/img/favicon.png" rel="icon">
        <link href="assets2/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets2/vendor/aos/aos.css" rel="stylesheet">
        <link href="assets2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets2/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets2/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets2/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="assets2/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets2/css/style.css" rel="stylesheet">

        <!-- =======================================================
            * Template Name: Maxim - v4.7.0
            * Template URL: https://bootstrapmade.com/maxim-free-onepage-bootstrap-theme/
            * Author: BootstrapMade.com
            * License: https://bootstrapmade.com/license/
            ======================================================== -->
    </head>

    <body>

        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top d-flex align-items-center" style="background-color: white">
            <div class="container d-flex justify-content-between">

                <div class="be-navbar-header">
                    <a href="index.html">
                        <img src="assets2\img\logoFPT.png" style="padding-left: 60px; width: 150px; height: 60px" />
                    </a>
                </div>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets2/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>

            <div class="container text-center text-md-left">
                <a href="registration.jsp" class="btn-get-started scrollto" style=" background: white; padding: 5px; border: 1px solid ; border-radius: 10px ">Register</a>
                <a href="login.jsp" class="btn-get-started scrollto" style=" background: white; padding: 5px;padding-left: 16px;padding-right: 16px; border: 1px solid green; border-radius: 10px">Login</a>

            </div>


        </header>
        <!-- End Header -->

        <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
            <div class="container text-center text-md-left" data-aos="fade-up">
                <h1>Social Constructive Learning</h1>
                <h2>Construct knowledge and personalize the learning way to empower learners' full potential.</h2>
                <a href="login.jsp" class="btn-get-started scrollto">Join now</a>
            </div>
        </section>
        <!-- End Hero -->

        <main id="main">

            <!-- ======= About Section ======= -->
            <section id="about" class="about">
                <div class="container">

                    <div class="row">
                        <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                            <img src="assets2/img/about-img.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                            <h3 data-aos="fade-up">Voluptatem dignissimos provident</h3>
                            <p data-aos="fade-up">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                            <div class="icon-box" data-aos="fade-up">
                                <i class="bx bx-receipt"></i>
                                <h4>Corporis voluptates sit</h4>
                                <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                            </div>

                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <i class="bx bx-cube-alt"></i>
                                <h4>Ullamco laboris nisi</h4>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>

                            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                                <i class="bx bx-cube-alt"></i>
                                <h4>Ullamco laboris nisi</h4>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>

                        </div>
                    </div>

                </div>
            </section>
            <!-- End About Section -->



            <!-- ======= Features Section ======= -->
            <section id="features" class="features">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-4 mb-5 mb-lg-0" data-aos="fade-right">
                            <ul class="nav nav-tabs flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-bs-toggle="tab" href="#tab-1">
                                        <h4>Modi sit est</h4>
                                        <p>Quis excepturi porro totam sint earum quo nulla perspiciatis eius.</p>
                                    </a>
                                </li>
                                <li class="nav-item mt-2">
                                    <a class="nav-link" data-bs-toggle="tab" href="#tab-2">
                                        <h4>Unde praesentium sed</h4>
                                        <p>Voluptas vel esse repudiandae quo excepturi.</p>
                                    </a>
                                </li>
                                <li class="nav-item mt-2">
                                    <a class="nav-link" data-bs-toggle="tab" href="#tab-3">
                                        <h4>Pariatur explicabo vel</h4>
                                        <p>Velit veniam ipsa sit nihil blanditiis mollitia natus.</p>
                                    </a>
                                </li>
                                <li class="nav-item mt-2">
                                    <a class="nav-link" data-bs-toggle="tab" href="#tab-4">
                                        <h4>Nostrum qui quasi</h4>
                                        <p>Ratione hic sapiente nostrum doloremque illum nulla praesentium id</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-7 ml-auto" data-aos="fade-left">
                            <div class="tab-content">
                                <div class="tab-pane active show" id="tab-1">
                                    <figure>
                                        <img src="assets2/img/features-1.png" alt="" class="img-fluid">
                                    </figure>
                                </div>
                                <div class="tab-pane" id="tab-2">
                                    <figure>
                                        <img src="assets2/img/features-2.png" alt="" class="img-fluid">
                                    </figure>
                                </div>
                                <div class="tab-pane" id="tab-3">
                                    <figure>
                                        <img src="assets2/img/features-3.png" alt="" class="img-fluid">
                                    </figure>
                                </div>
                                <div class="tab-pane" id="tab-4">
                                    <figure>
                                        <img src="assets2/img/features-4.png" alt="" class="img-fluid">
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- End Features Section -->

        </main>
        <!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer class="page-footer bg-image" style="padding-left: 200px; background-color: #ffecb5;">
            <div class="container">
                <div class="row mb-5">


                    <div class="col-lg-3 py-3">
                        <h5>Contact Us</h5>
                        <p>FPT University, HaNoi, VietNam</p>
                        <a href="#" class="footer-link">+00 1122 3344 5566</a>
                        <a href="#" class="footer-link">daihocfpt@fpt.edu.vn</a>

                    </div>
                    <div class="col-lg-3 py-3">
                        <h5>Company</h5>
                        <ul class="footer-menu">
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Hỏi đáp</a></li>
                            <li><a href="#">Chính sách và bảo mật</a></li>

                        </ul>
                    </div>
                    <div class="col-lg-3 py-3">
                        <h5>Liên hệ cần hỗ trợ hoặc nhận thông tin</h5>
                        <p>Get updates, news or events on your mail.</p>
                        <form action="#">
                            <input type="text" class="form-control" placeholder="Enter your email..">
                            <button type="submit" class="btn btn-success btn-block mt-2">Subscribe</button>
                        </form>
                    </div>

                </div>


            </div>
        </footer>
        <!-- End Footer -->

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

        <!-- Vendor JS Files -->
        <script src="assets2/vendor/aos/aos.js"></script>
        <script src="assets2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets2/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="assets2/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="assets2/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="assets2/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets2/js/main.js"></script>

    </body>

</html>
