<%-- 
    Document   : passwordSetup
    Created on : May 26, 2022, 10:29:58 AM
    Author     : DoXuanHieu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets\img\logo-fav.png">
        <title>Setup Password</title>
        <link rel="stylesheet" type="text/css" href="assets\lib\perfect-scrollbar\css\perfect-scrollbar.css">
        <link rel="stylesheet" type="text/css" href="assets\lib\material-design-icons\css\material-design-iconic-font.min.css">
        <link rel="stylesheet" href="assets\css\app.css" type="text/css">
    </head>

    <body class="be-splash-screen">
        <div class="be-wrapper be-login be-signup">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="splash-container sign-up">
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header"><img class="logo-img" src="assets\img\logo-xx.png" alt="logo" width="102" height="27"></div>
                            <div class="card-body">
                                <form action="RegistrationController" method="post">
                                <div>
                                    <div class="form-group">
                                        <input class="form-control" id="pass1" type="password" name="password" required="" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="pass2" type="password" name="password1" required="" placeholder="Password" onchange="check_pass()">
                                    </div>  
                                    <span id='message'></span>
                                </div>
                                <div class="form-group pt-2">
                                    <button class="btn btn-block btn-primary btn-xl" id="submit" name="btn_setpassword" type="submit" value="Set Password">Set Password</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="splash-footer">&copy; 2018 Your Company</div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets\lib\jquery\jquery.min.js" type="text/javascript"></script>
        <script src="assets\lib\perfect-scrollbar\js\perfect-scrollbar.min.js" type="text/javascript"></script>
        <script src="assets\lib\bootstrap\dist\js\bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="assets\js\app.js" type="text/javascript"></script>
        <script type="text/javascript">
                                            $(document).ready(function () {
                                                //-initialize the javascript
                                                App.init();
                                            });
        </script>
    </body>

</html>
